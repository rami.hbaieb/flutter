import 'package:flutter/material.dart';
import 'detailPage.dart';
class ProductInfo extends StatelessWidget {

  final String _image;
  final String _title;
  final int _price;


  const ProductInfo(this._image, this._title, this._price);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: Card(
        child: InkWell(
          onTap: (){
            Navigator.push(context, MaterialPageRoute(builder: (context)=>DetailPage()));
          },
          child: Row(
            children: [
              Container(
                margin: const EdgeInsets.fromLTRB(10, 10, 20, 10),
                  child: Image.asset(_image, width: 200, height: 94,)
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(_title),
                  Text(_price.toString() + "TND", textScaleFactor: 2,)
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
