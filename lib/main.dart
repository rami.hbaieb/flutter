import 'package:flutter/material.dart';
import 'product_info.dart';
void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "G-Store Esprit",
      home: Scaffold(
        appBar: AppBar(
          title: const Text("G-Store Esprit"),
        ),
          body: Column(
            children: const [
              ProductInfo("assets/images/dmc5.jpg", "Devil May Cry 5", 200),
              ProductInfo("assets/images/re8.jpg", "Resident Evil Village", 200),
              ProductInfo("assets/images/nfs.jpg", "Need For Speed", 100),
              ProductInfo("assets/images/rdr2.jpg", "Red Dead Redemption II", 150),
              ProductInfo("assets/images/fifa.jpg", "FIFA 22", 100)
            ],
          )
      )
    );
  }

}





