import 'package:flutter/material.dart';
import 'inscription.dart';
class DetailPage extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Devil May Cry 5"),
      ),
      body: Column(
        children: [
          Container(
              margin: const EdgeInsets.fromLTRB(20, 20, 20, 10),
              child: Image.asset("assets/images/dmc5.jpg")),
          Container(
              margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
              child: Text("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.")),
          Container(
              margin: const EdgeInsets.fromLTRB(10, 50, 10, 10),
              child: Text("200 TND", textScaleFactor: 2,)),
          Container(
            margin: const EdgeInsets.fromLTRB(10, 40, 10, 10),
            child: ElevatedButton.icon(
              icon: Icon(
                Icons.shopping_basket_outlined,
                size: 24.0,
              ),
              label: Text('Acheter'),
               onPressed: () {
                 Navigator.push( context, MaterialPageRoute(builder: (context)=> Inscription()));
               print('Pressed');
               },

    ),
    )

        ],
      ),
    );
  }
}
